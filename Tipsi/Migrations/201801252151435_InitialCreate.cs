namespace Tipsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Features",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Paznokcie = c.Boolean(nullable: false),
                        Masaz = c.Boolean(nullable: false),
                        Piling = c.Boolean(nullable: false),
                        Henna = c.Boolean(nullable: false),
                        Marker_MarkerId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Markers", t => t.Marker_MarkerId)
                .Index(t => t.Marker_MarkerId);
            
            CreateTable(
                "dbo.Markers",
                c => new
                    {
                        MarkerId = c.String(nullable: false, maxLength: 128),
                        category = c.String(),
                        title = c.String(),
                        location = c.String(),
                        latitude = c.Double(nullable: false),
                        longitude = c.Double(nullable: false),
                        url = c.String(),
                        type = c.String(),
                        type_icon = c.String(),
                        rating = c.Int(nullable: false),
                        date_created = c.String(),
                        price = c.String(),
                        featured = c.Int(nullable: false),
                        color = c.String(),
                        person_id = c.Int(nullable: false),
                        year = c.Int(nullable: false),
                        special_offer = c.Int(nullable: false),
                        description = c.String(),
                        last_review = c.String(),
                        last_review_rating = c.Int(nullable: false),
                        pleaceDescription = c.String(),
                        name = c.String(),
                        phone_number = c.String(),
                        mobile_number = c.String(),
                        email = c.String(),
                        website = c.String(),
                        ApplicationUserID = c.String(),
                        status = c.Boolean(nullable: false),
                        item_specific_Id = c.Int(),
                    })
                .PrimaryKey(t => t.MarkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.MarkerId)
                .ForeignKey("dbo.Item_Specific", t => t.item_specific_Id)
                .Index(t => t.MarkerId)
                .Index(t => t.item_specific_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserDescription = c.String(),
                        UnConfirmedEmail = c.String(maxLength: 256),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ProfilePics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        Path = c.String(),
                        Size = c.Long(nullable: false),
                        CurrentUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentUser_Id)
                .Index(t => t.CurrentUser_Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Item_Specific",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        bedrooms = c.Int(nullable: false),
                        bathrooms = c.Int(nullable: false),
                        rooms = c.Int(nullable: false),
                        garages = c.Int(nullable: false),
                        area = c.Int(nullable: false),
                        menu = c.String(),
                        offer1 = c.String(),
                        offer1_price = c.String(),
                        offer2 = c.String(),
                        offer2_price = c.String(),
                        offer3 = c.String(),
                        offer3_price = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Galleries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        Path = c.String(),
                        Size = c.Long(nullable: false),
                        isProfilePic = c.Boolean(nullable: false),
                        CurrentMarkerId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Markers", t => t.CurrentMarkerId, cascadeDelete: true)
                .Index(t => t.CurrentMarkerId);
            
            CreateTable(
                "dbo.OpenHours",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        mondayFrom = c.String(),
                        mondayTo = c.String(),
                        isSelectedMon = c.Boolean(nullable: false),
                        tuesdayFrom = c.String(),
                        tuesdayTo = c.String(),
                        isSelectedTue = c.Boolean(nullable: false),
                        wednesdayFrom = c.String(),
                        wednesdayTo = c.String(),
                        isSelectedWed = c.Boolean(nullable: false),
                        thursdayFrom = c.String(),
                        thursdayTo = c.String(),
                        isSelectedThu = c.Boolean(nullable: false),
                        fridayFrom = c.String(),
                        fridayto = c.String(),
                        isSelectedFri = c.Boolean(nullable: false),
                        saturdayFrom = c.String(),
                        saturdayTo = c.String(),
                        isSelectedSat = c.Boolean(nullable: false),
                        sundayFrom = c.String(),
                        sundayTo = c.String(),
                        isSelectedSun = c.Boolean(nullable: false),
                        Marker_MarkerId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Markers", t => t.Marker_MarkerId)
                .Index(t => t.Marker_MarkerId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Features", "Marker_MarkerId", "dbo.Markers");
            DropForeignKey("dbo.OpenHours", "Marker_MarkerId", "dbo.Markers");
            DropForeignKey("dbo.Galleries", "CurrentMarkerId", "dbo.Markers");
            DropForeignKey("dbo.Markers", "item_specific_Id", "dbo.Item_Specific");
            DropForeignKey("dbo.Markers", "MarkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProfilePics", "CurrentUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.OpenHours", new[] { "Marker_MarkerId" });
            DropIndex("dbo.Galleries", new[] { "CurrentMarkerId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.ProfilePics", new[] { "CurrentUser_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Markers", new[] { "item_specific_Id" });
            DropIndex("dbo.Markers", new[] { "MarkerId" });
            DropIndex("dbo.Features", new[] { "Marker_MarkerId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.OpenHours");
            DropTable("dbo.Galleries");
            DropTable("dbo.Item_Specific");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.ProfilePics");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Markers");
            DropTable("dbo.Features");
        }
    }
}
