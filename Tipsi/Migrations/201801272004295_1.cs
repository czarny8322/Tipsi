namespace Tipsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Markers", "dateAdded", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Markers", "dateAdded");
        }
    }
}
