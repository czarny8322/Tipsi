namespace Tipsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Messages", "ProfilePicFrom");
            DropColumn("dbo.Messages", "ProfilePicTo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Messages", "ProfilePicTo", c => c.String());
            AddColumn("dbo.Messages", "ProfilePicFrom", c => c.String());
        }
    }
}
