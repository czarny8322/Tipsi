namespace Tipsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "ProfilePicFrom", c => c.String());
            AddColumn("dbo.Messages", "ProfilePicTo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "ProfilePicTo");
            DropColumn("dbo.Messages", "ProfilePicFrom");
        }
    }
}
