namespace Tipsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Markers", "date_created", c => c.DateTime());
            DropColumn("dbo.Markers", "dateAdded");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Markers", "dateAdded", c => c.DateTime());
            AlterColumn("dbo.Markers", "date_created", c => c.String());
        }
    }
}
