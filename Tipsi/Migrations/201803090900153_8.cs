namespace Tipsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessageModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        MessageBody = c.String(),
                        ParrentMessageId = c.Int(nullable: false),
                        IsViewed = c.Boolean(nullable: false),
                        SenderId = c.String(),
                        ReceivedID = c.String(),
                        CreatedDate = c.DateTime(),
                        ViewedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MessageModels");
        }
    }
}
