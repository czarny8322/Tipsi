﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tipsi.Models
{
    public class JsonMarkersModel
    {        
        public List<Marker> data { get; set; }        
    }

    public class Marker
    {
        public string MarkerId { get; set; }
        public string category { get; set; }        
        public string title { get; set; }        
        public string location { get; set; }        
        public double latitude { get; set; }              
        public double longitude { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public string type_icon { get; set; }
        public int rating { get; set; }
        public virtual List<Gallery> list_gallery { get; set; }               
        public List<string> gallery { get; set; }        
        public DateTime? date_created { get; set; }
        public string price { get; set; }
        public int featured { get; set; }
        public string color { get; set; }
        public int person_id { get; set; }
        public int year { get; set; }
        public int special_offer { get; set; }
        public Item_Specific item_specific { get; set; }
        public virtual Features features { get; set; }
        public virtual OpenHours open_hours { get; set; }
        public string description { get; set; }
        public string last_review { get; set; }
        public int last_review_rating { get; set; }
        public string pleaceDescription { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
        public string mobile_number { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        
        public string ApplicationUserID { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public bool status { get; set; }
    }

    public class Features
    {        
        public int Id { get; set; }

        public bool Paznokcie { get; set; }
        public bool Masaz { get; set; }
        public bool Piling { get; set; }
        public bool Henna { get; set; }
        
        public virtual Marker Marker { get; set; }
    }

    public class Gallery
    {
        public int Id { get; set; }
                
        public string FileName { get; set; }
        public string Path { get; set; }
        public long Size { get; set; }
        public bool isProfilePic { get; set; }

        public string CurrentMarkerId { get; set; }
        public virtual Marker CurrentMarker { get; set; }
    }
        
    public class OpenHours
    {
        public int Id { get; set; }

        public string mondayFrom { get; set; }
        public string mondayTo { get; set; }
        public bool isSelectedMon { get; set; }

        public string tuesdayFrom { get; set; }
        public string tuesdayTo { get; set; }
        public bool isSelectedTue { get; set; }

        public string wednesdayFrom { get; set; }
        public string wednesdayTo { get; set; }
        public bool isSelectedWed { get; set; }

        public string thursdayFrom { get; set; }
        public string thursdayTo { get; set; }
        public bool isSelectedThu { get; set; }

        public string fridayFrom { get; set; }
        public string fridayto { get; set; }
        public bool isSelectedFri { get; set; }

        public string saturdayFrom { get; set; }
        public string saturdayTo { get; set; }
        public bool isSelectedSat { get; set; }

        public string sundayFrom { get; set; }
        public string sundayTo { get; set; }
        public bool isSelectedSun { get; set; }

        public virtual Marker Marker { get; set; }
    }

    public class Item_Specific
    {
        public int Id { get; set; }
        public int bedrooms { get; set; }
        public int bathrooms { get; set; }
        public int rooms { get; set; }
        public int garages { get; set; }
        public int area { get; set; }
        public string menu { get; set; }
        public string offer1 { get; set; }
        public string offer1_price { get; set; }
        public string offer2 { get; set; }
        public string offer2_price { get; set; }
        public string offer3 { get; set; }
        public string offer3_price { get; set; }
    }
}