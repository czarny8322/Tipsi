﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using static Tipsi.Models.TipsiDbContext;
using System;

namespace Tipsi.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public string UserDescription  { get; set; }
        public virtual ProfilePic ProfilePic { get; set; }
        public virtual Marker Marker { get; set; }


        [MaxLength(256)]
        public string UnConfirmedEmail { get; set; }
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class TipsiDbContext : IdentityDbContext<ApplicationUser>
    {
        public TipsiDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            
        }
        
        public DbSet<Marker> Markers { get; set; }       
        public DbSet<Features> Features { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<OpenHours> OpenHours { get; set; }
        public DbSet<ProfilePic> ProfilePictures { get; set; }
        public DbSet<MessageModel> MessageModels { get; set; }


        public static TipsiDbContext Create()
        {
            return new TipsiDbContext();
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Marker>()
                .HasKey(s => s.MarkerId)
                .HasRequired(s => s.ApplicationUser);

            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(s => s.Marker);


            modelBuilder.Entity<ApplicationUser>()
               .HasOptional(s => s.ProfilePic) // Mark Address property optional in Student entity
               .WithRequired(ad => ad.CurrentUser); // mark Student property as required in StudentAddress entity. Cannot save StudentAddress without Student


            modelBuilder.Entity<Features>()
                .HasKey(s => s.Id)
                .HasRequired(s => s.Marker);

            modelBuilder.Entity<Marker>()
                .HasOptional(s => s.features);

            
            modelBuilder.Entity<OpenHours>()
                .HasKey(s => s.Id)
                .HasRequired(s => s.Marker);

            modelBuilder.Entity<Marker>()
                .HasOptional(s => s.open_hours);


            modelBuilder.Entity<Marker>()
                .HasMany<Gallery>(g => g.list_gallery)
                .WithRequired(s => s.CurrentMarker)
                .HasForeignKey(s => s.CurrentMarkerId)
                .WillCascadeOnDelete();
            
        }

        public class ProfilePic
        {
            public int Id { get; set; }
            public string FileName { get; set; }
            public string Path { get; set; }
            public long Size { get; set; }

            public virtual ApplicationUser CurrentUser { get; set; }
        }

        public class MessageModel
        {
            public int ID { get; set; }
            public string Subject { get; set; }
            public string MessageBody { get; set; }
            public int ParrentMessageId { get; set; }
            public bool IsViewed { get; set; }
            public string SenderId { get; set; }
            public string ReceivedID { get; set; }
            public DateTime? CreatedDate { get; set; }
            public DateTime? ViewedDate { get; set; }
        }


    }
    
}