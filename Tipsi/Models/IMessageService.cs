﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Tipsi.Models.TipsiDbContext;

namespace Tipsi.Models
{
    public interface IMessageService
    {
        void SendMessage(MessageModel messageModel);
        List<MessageModel> GetAllCoversationsForUser(int userId);
        int GetNewMessagesCount(int userId);
        void SetMessageViewed(int messageId);
    }
}