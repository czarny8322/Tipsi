﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tipsi.ViewModels
{
    public class PasswordViewModel
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}