﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tipsi.ViewModels
{
    public class ProfileViewModel
    {
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
        public bool _emailStatus { get; set; }
    }
}