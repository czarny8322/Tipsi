﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Tipsi.Models;
using static Tipsi.Models.TipsiDbContext;

namespace Tipsi.Controllers
{
    public class MapController : Controller, IMessageService
    {

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        public ActionResult SendMessages(Messages model)
        {
            if (model.From != model.To)
            {               
                return Json("Wiadomość została wysłana");
            }
            else
            {
                return Json("Brak możliwości wysłania wiadomości do siebie");
            }
            //return Json("Coś poszło nie tak");
        }
        
        public class Messages
        {
            public string From { get; set; }
            public string To { get; set; }
            public string NameFrom { get; set; }
            public string NameTo { get; set; }
            public string Message { get; set; }            
        }

        
        // GET: Map
        public ActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public JsonResult Test()
        //{

        //    var item_specific = new Item_Specific()
        //    {
        //        bedrooms = 2,
        //        bathrooms = 3,
        //        rooms = 5,
        //        garages = 1,
        //        area = 333
        //    };

        //    var marker = new Marker()
        //    {

        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.2296756,
        //        longitude = 21.0122287,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker2 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.2296756,
        //        longitude = 21.1122287,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker3 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.3296756,
        //        longitude = 21.1122287,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker4 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.2296756,
        //        longitude = 21.4122287,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker5 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.2256756,
        //        longitude = 21.1122287,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker6 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.2296756,
        //        longitude = 21.1162287,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker7 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria",
        //        title = "tytuł",
        //        location = "adress",
        //        latitude = 52.2296756,
        //        longitude = 21.1122787,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" },
        //        features = new string[] {
        //            "Free Parking",
        //            "Cards Accepted",
        //            "Wi-Fi",
        //            "Air Condition",
        //            "Reservations",
        //            "Teambuildings",
        //            "Places to seat"},
        //        date_created = "2014-11-03",
        //        price = "$2500",
        //        featured = 0,
        //        color = "",
        //        person_id = 1,
        //        year = 1980,
        //        special_offer = 0,
        //        item_specific = item_specific,
        //        description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };

        //    var marker8 = new Marker()
        //    {
        //        MarkerId = "sdasd",
        //        category = "kategoria1",
        //        title = "NOWY",
        //        location = "adress",
        //        latitude = 52.2496756,
        //        longitude = 21.1122787,
        //        url = "item-detail.html",
        //        type = "Apartament",
        //        type_icon = "assets/icons/store/apparel/umbrella-2.png",
        //        //rating = 4,
        //        gallery = new string[] { "assets/img/items/1.jpg", "assets/img/items/5.jpg", "assets/img/items/4.jpg" }
        //        //features = new string[] {
        //        //    "Free Parking",
        //        //    "Cards Accepted",
        //        //    "Wi-Fi",
        //        //    "Air Condition",
        //        //    "Reservations",
        //        //    "Teambuildings",
        //        //    "Places to seat"},
        //        //date_created = "2014-11-03",
        //        //price = "$2500",
        //        //featured = 0,
        //        //color = "",
        //        //person_id = 1,
        //        //year = 1980,
        //        //special_offer = 0,
        //        //item_specific = item_specific,
        //        //description = "OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS, OPIS"
        //    };
        //    //GetMarkers();
        //    //var dataObject = new JsonMarkersModel() { data = new Marker { marker, marker2, marker3, marker4, marker5, marker6, marker7, marker8 } };
        //    var dataObject = new JsonMarkersModel() { data = new List<Marker>() };
        //    dataObject.data.Add(marker);
        //    dataObject.data.Add(marker4);
        //    dataObject.data.Add(marker2);
        //    dataObject.data.Add(marker3);

        //    var jsonData = new JavaScriptSerializer().Serialize(dataObject);

        //    return Json(GetMarkers(), JsonRequestBehavior.AllowGet);
        //}

        public string GetMarkers()
        {
            var dataObject = new JsonMarkersModel() { data = new List<Marker>() };
            string jsonObjectMarkers = string.Empty;

            using (TipsiDbContext db = new TipsiDbContext())
            {
                var markersFromDb = db.Markers.Where(x => x.status == true).ToList();

                //var markersFromDb = db.Markers.ToList();

                foreach (var markerDb in markersFromDb)
                {
                    var marker = new Marker();

                    marker.MarkerId = markerDb.MarkerId;
                    marker.category = markerDb.category;
                    marker.title = markerDb.title;
                    marker.location = markerDb.location;
                    marker.latitude = markerDb.latitude;
                    marker.longitude = markerDb.longitude;
                    //marker.url = markerDb.url;
                    marker.url = string.Format("map/Item_detail?UserID={0}", markerDb.MarkerId);
                    //marker.url = "www.wp.pl";

                    marker.type = markerDb.category;
                    //marker.type_icon = "assets/icons/store/apparel/umbrella-2.png";
                    marker.type_icon = markerDb.type_icon;
                    //marker.gallery = new string[] { "assets/icons/store/apparel/umbrella-2.png", "assets/img/items/5.jpg", "assets/img/items/4.jpg" };
                    //marker.gallery = new List<string>() { "/assets/icons/store/apparel/umbrella-2.png", "/assets/icons/store/apparel/bags.png", "/assets/icons/store/apparel/hats.png" };

                    if (markerDb.list_gallery.Count() < 1)
                    {
                        marker.gallery = new List<string>() { "assets/img/beauty.png" };
                    }
                    else
                        marker.gallery = markerDb.list_gallery.Select(m => m.Path).ToList();

                    dataObject.data.Add(marker);
                    // jsonObjectMarkers.data = markers;
                }
                jsonObjectMarkers = new JavaScriptSerializer().Serialize(dataObject);
            }

            return jsonObjectMarkers;
        }

        public async Task<ActionResult> Item_detail(string UserID)
        {
            var userIdentity = User.Identity;
            var myId = userIdentity.GetUserId();  

            var user = await UserManager.FindByIdAsync(UserID);

            var currentUser = await UserManager.FindByIdAsync(myId);

            if (user != null)
            {

                //ViewBag.UserProfilePic = "";
                //ViewBag.MyProfilePic = "";

                //using (TipsiDbContext context = new TipsiDbContext())
                //{                    
                //    var userPic = context.ProfilePictures.Where(x => x.CurrentUser.Id == UserID).Select(x => x.Path).FirstOrDefault();
                //    if(userPic != null)
                //    {
                //        ViewBag.UserProfilePic = userPic;
                //    }
                //    else
                //    {
                //        ViewBag.UserProfilePic = "/assets/img/profile_default.png";
                //    }

                //    var myPic = context.ProfilePictures.Where(x => x.CurrentUser.Id == myId).Select(x => x.Path).FirstOrDefault();
                //    if (myPic != null)
                //    {
                //        ViewBag.MyProfilePic = myPic;
                //    }
                //    else
                //    {
                //        ViewBag.MyProfilePic = "/assets/img/profile_default.png";
                //    }
                //}                   

                ViewBag.UserEmail = user.Email;
                
                //ViewBag.MyID = myId;

                if (currentUser != null)
                {
                    ViewBag.MyEmail = currentUser.Email;
                    ViewBag.MyName = currentUser.UserName;                    

                    if (currentUser.Marker == null)
                    {
                        ViewBag.NameShopFrom = "Niezarejestrowany";
                    }
                    ViewBag.NameShopFrom = currentUser.Marker.title;
                }
                else
                {
                    ViewBag.NameShopFrom = "Niezarejestrowany";
                }
                

                return View(user.Marker);
            }
            return Redirect("/map");
        }


        public string GetLastMarkers()
        {
            var dataObject = new JsonMarkersModel() { data = new List<Marker>() };
            string jsonObjectMarkers = string.Empty;

            using (TipsiDbContext db = new TipsiDbContext())
            {
                var markersFromDb = db.Markers.Where(x => x.status == true).OrderByDescending(x => x.date_created).Take(4).ToList();
                foreach (var markerDb in markersFromDb)
                {
                    var marker = new Marker();

                    marker.MarkerId = markerDb.MarkerId;
                    marker.category = markerDb.category;
                    marker.title = markerDb.title;
                    marker.location = markerDb.location;
                    marker.latitude = markerDb.latitude;
                    marker.longitude = markerDb.longitude;
                    marker.url = string.Format("map/Item_detail?UserID={0}", markerDb.MarkerId);
                    marker.type_icon = markerDb.type_icon;
                    marker.pleaceDescription = markerDb.pleaceDescription;

                    if (markerDb.list_gallery.Count() < 1)
                    {
                        marker.gallery = new List<string>() { "assets/img/beauty.png" };
                    }
                    else
                        marker.gallery = markerDb.list_gallery.Select(m => m.Path).Take(1).ToList();

                    dataObject.data.Add(marker);
                    // jsonObjectMarkers.data = markers;
                }
                jsonObjectMarkers = new JavaScriptSerializer().Serialize(dataObject);
            }

            return jsonObjectMarkers;
        }

        public string GetModalMarker(string id)
        {
            //var dataObject = new JsonMarkersModel() { data = new List<Marker>() };
            string jsonObjectMarkers = string.Empty;

            using (TipsiDbContext db = new TipsiDbContext())
            {
                var markerDb = db.Markers.Where(x => x.status == true).Where(x => x.MarkerId == id).FirstOrDefault();

                if (markerDb != null)
                {
                    var marker = new Marker();

                    marker.MarkerId = markerDb.MarkerId;
                    marker.category = markerDb.category;
                    marker.title = markerDb.title;
                    marker.location = markerDb.location;
                    marker.latitude = markerDb.latitude;
                    marker.longitude = markerDb.longitude;
                    marker.url = string.Format("map/Item_detail?UserID={0}", markerDb.MarkerId);
                    marker.type_icon = markerDb.type_icon;
                    marker.pleaceDescription = markerDb.pleaceDescription;
                    marker.phone_number = markerDb.phone_number;
                    marker.mobile_number = markerDb.mobile_number;

                    if (markerDb.list_gallery.Count() < 1)
                    {
                        marker.gallery = new List<string>() { "assets/img/beauty.png" };
                    }
                    else
                        marker.gallery = markerDb.list_gallery.Select(m => m.Path).ToList();

                    //marker.features = featuresMap(markerDb.features);
                    db.Configuration.ProxyCreationEnabled = false;

                    marker.features = db.Features.Where(x => x.Marker.MarkerId == id).FirstOrDefault();
                    marker.features.Marker = null;

                    marker.open_hours = db.OpenHours.Where(x => x.Marker.MarkerId == id).FirstOrDefault();
                    marker.open_hours.Marker = null;



                    jsonObjectMarkers = new JavaScriptSerializer().Serialize(marker);
                }

            }
            return jsonObjectMarkers;
        }

        
        private Features featuresMap(Features features)
        {
            Features _features = new Features();

            _features.Henna = features.Henna;
            _features.Masaz = features.Masaz;
            _features.Paznokcie = features.Paznokcie;
            _features.Piling = features.Piling;

            return _features;
        }

        public void SendMessage(MessageModel messageModel)
        {
            using (TipsiDbContext context = new TipsiDbContext())
            {
                var prevMessage = context.MessageModels.Where(p => (p.ReceivedID == messageModel.ReceivedID && p.SenderId == messageModel.SenderId) || (p.ReceivedID == messageModel.SenderId && p.SenderId == messageModel.ReceivedID));

                context.MessageModels.Add(messageModel);
                context.SaveChanges();
            }

            throw new NotImplementedException();
        }

        //https://www.codementor.io/sovitpoudel/implementing-inbox-messaging-system-in-asp-net-arqk8mv58

        public List<MessageModel> GetAllCoversationsForUser(string userId)
        {
            using (TipsiDbContext context = new TipsiDbContext())
            {
                var messages = context.MessageModels.Where(p => p.SenderId == userId || p.ReceivedID == userId).OrderByDescending(p => p.ID);
            }                
            throw new NotImplementedException();
        }

        private readonly List<MessageModel> messageHistory = new List<MessageModel>();
        private void GetChildMessages(int messageId, List<MessageModel> lsMessages, int currentUserId)
        {
            var childMsg = lsMessages.FirstOrDefault(p => p.ParentMessageId == messageId);
            if (childMsg != null)
            {
                messageHistory.Add(childMsg);
                GetChildMessages(childMsg.MessageId, lsMessages, currentUserId);
            }
        }



        public int GetNewMessagesCount(int userId)
        {
            throw new NotImplementedException();
        }

        public void SetMessageViewed(int messageId)
        {
            throw new NotImplementedException();
        }
    }
}