﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Tipsi.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Tipsi.Controllers;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.IO;
using AutoMapper;
using System.Threading.Tasks;
using Tipsi.ViewModels;
using static Tipsi.Models.TipsiDbContext;

namespace Tipsi.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        //private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        private DirectoryInfo _originalDirectory;
        public DirectoryInfo OriginalDirectory
        {
            get
            {
                return _originalDirectory ?? new DirectoryInfo(string.Format("{0}Images\\WallImages", Server.MapPath(@"\")));
            }
            private set
            {
                _originalDirectory = value;
            }
        }
        TipsiDbContext context;
        public UsersController()
        {
            context = new TipsiDbContext();
        }

        // GET: Users
        public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                TipsiDbContext context = new TipsiDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var s = UserManager.GetRoles(user.GetUserId());

                var ifIsNull = s.FirstOrDefault(x => x != null);

                if (ifIsNull != null)
                {
                    if (s[0].ToString() == "Admin")
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        public ActionResult Index()
        {
            //if (User.Identity.IsAuthenticated)
            //{
            //    var user = User.Identity;
            //    ViewBag.Name = user.Name;

            //    TipsiDbContext context = new TipsiDbContext();
            //    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            //    var s = UserManager.GetRoles(user.GetUserId());


            //    var user1 = UserManager.FindById(User.Identity.GetUserId());
            //    ViewBag.Email = user1.Email;


            //    ViewBag.displayMenu = "No";

            //    if (isAdminUser())
            //    {
            //        ViewBag.displayMenu = "Yes";
            //    }
            //    return View();
            //}
            //else
            //{
            //    ViewBag.Name = "Not Logged IN";
            //}
            //return View();
            return RedirectToAction("Index", "map");
        }
        

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //    return RedirectToAction("Index", "Home");
        //} 

        public async Task<ActionResult> MyProfile()
        {
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (currentUser != null)
            {
                var profileModel = new ProfileViewModel()
                {
                    Name = currentUser.UserName,
                    Email = currentUser.Email,
                    Phone = currentUser.PhoneNumber,
                    Description = currentUser.UserDescription,
                    UserId = currentUser.Id,
                    _emailStatus = false          
                };
                return View(profileModel);
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> MyProfile(ProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                
                if (model.UserId == currentUser.Id)
                {
                    if (currentUser != null)
                    {
                        //check if email was changed
                        if (model.Email != currentUser.Email)
                        {
                            //doing a quick swap so we can send the appropriate confirmation email
                            currentUser.UnConfirmedEmail = model.Email;
                            currentUser.EmailConfirmed = false;
                            var result = await UserManager.UpdateAsync(currentUser);

                            if (result.Succeeded)
                            {
                                string callbackUrl =
                                await SendEmailConfirmationWarningAsync(currentUser.Id, "Confirm your new email");
                                model._emailStatus = true;
                            }
                            else
                            {
                                ModelState.AddModelError(string.Empty, "Próba zmiany adresu email nie powiodła się");                                
                            }
                        }
                    }

                    currentUser.UserName = model.Name;
                    currentUser.PhoneNumber = model.Phone;
                    currentUser.UserDescription = model.Description;

                    await UserManager.UpdateAsync(currentUser);
                }
            };
            return View(model);

            //https://stackoverflow.com/questions/25570025/net-identity-email-username-change
        }
        
        public ActionResult Password()
        {            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Password(string UserId, string Password, string Confirm_password)
        {
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if ((Password == Confirm_password) && (UserId == currentUser.Id))
            {
                var status = await UserManager.RemovePasswordAsync(UserId);
                if (status.Succeeded)
                {
                    status = await UserManager.AddPasswordAsync(UserId, Password);
                    if (status.Succeeded)
                    {
                        return Json(true);
                    }
                }
            }
            return View(false);
        }

        //public async Task<ActionResult> Messages()
        //{
        //    var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    ViewBag.MyEmail = currentUser.Email;

        //    var messageList = new List<ListMessages>();

        //    using (TipsiDbContext context = new TipsiDbContext())
        //    {
        //        var messagesSendsFromMe = context.messages.Where(x => x.From == currentUser.Email).ToList();
        //        var messagesSendsToMe = context.messages.Where(x => x.To == currentUser.Email).ToList();

        //        var sent = messagesSendsFromMe.Select(x => x.To).ToList();
        //        var received = messagesSendsToMe.Select(x => x.From).ToList();

        //        sent.AddRange(received);
        //        var allUsersForMessages = sent.Distinct();                

        //        foreach (var i in allUsersForMessages)
        //        {
        //            var sentToUser = messagesSendsFromMe.Where(x => x.To == i).ToList();
        //            var receivedFromUser = messagesSendsToMe.Where(x => x.From == i).ToList();

        //            sentToUser.AddRange(receivedFromUser);

        //            var ProfilePic = getPictureForUser(i);

        //            messageList.Add(new ListMessages() {messages = sentToUser.OrderBy(x=>x.Date).ToList(), ProfilePic = ProfilePic });
        //        }
        //    }
        //  return View(messageList);
        //}

        private string getPictureForUser(string i)
        {
            string picturePath = "";

            using (TipsiDbContext context = new TipsiDbContext())
            {
                picturePath = context.ProfilePictures.Where(x => x.CurrentUser.Email == i).Select(x => x.Path).FirstOrDefault();

                if (picturePath == null)
                {
                    picturePath = "/assets/img/profile_default.png";
                }
            }
            return picturePath;
        }

        public ActionResult MyItems()
        {
            var myItem = new Marker();

            using (TipsiDbContext context = new TipsiDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var currentUser = UserManager.FindById(User.Identity.GetUserId());

                myItem = currentUser.Marker;
                ViewBag.Picture = context.Galleries.Where(x => x.CurrentMarkerId == currentUser.Id).Select(x => x.Path).Take(1).FirstOrDefault();
                ViewBag.Id = User.Identity.GetUserId();
            }
            return View(myItem);
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword_Profile(string userID, string password)
        {
           var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());

           if (!(string.IsNullOrEmpty(password) || string.IsNullOrEmpty(userID)) || (currentUser.Id == userID))
            {   
                var result = await UserManager.RemovePasswordAsync(userID);
                if (result.Succeeded)
                {
                    result = await UserManager.AddPasswordAsync(userID, password);
                    if (result.Succeeded)
                    {
                        return Json(true);
                    }                        
                }
                else
                {
                    return Json(false);
                }
            }
           return Json(false);
        }

         public ActionResult AddLocalization()
        {
            Marker marker = new Marker();
            //marker.open_hours = GetOpenHours();
            return View(marker);
        }

        [HttpPost]
        public ActionResult AddLocalization(Marker jsonModel)
        {            
            if (ModelState.IsValid)
            {
                using (TipsiDbContext db = new TipsiDbContext())
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                    var currentUser = UserManager.FindById(User.Identity.GetUserId());
                    
                    if (currentUser.Marker == null)
                    {                                              
                        jsonModel.MarkerId = currentUser.Id;
                        jsonModel.date_created = DateTime.Now;
                        jsonModel.status = true;
                        jsonModel.type_icon = setIconType(jsonModel.category);
                        db.Markers.Add(jsonModel);
                        db.SaveChanges();
                    }
                    else
                    {
                        var _features = db.Features.Where(m => m.Marker.MarkerId == currentUser.Id).FirstOrDefault();
                        if (_features != null)
                            db.Features.Remove(_features);

                        var _openHours = db.OpenHours.Where(m => m.Marker.MarkerId == currentUser.Id).FirstOrDefault();
                        if (_openHours != null)
                            db.OpenHours.Remove(_openHours);

                        jsonModel.status = true;
                        jsonModel.date_created = DateTime.Now;
                        jsonModel.type_icon = setIconType(jsonModel.category);

                        mapMyModelToDatabase(jsonModel, currentUser);
                       
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("MyItems", "Users");
            }
            return View(jsonModel);
        }

       

        public ActionResult EditLocalization()
        {
            Marker jsonModel = new Marker();
            using (TipsiDbContext context = new TipsiDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var currentUser = UserManager.FindById(User.Identity.GetUserId());
                jsonModel = mapMyModelToView(currentUser, jsonModel);
                //jsonModel.open_hours = GetOpenHours();                               
            }
            return View(jsonModel);
        }

        [HttpPost]
        public ActionResult EditLocalization(Marker jsonModel)
        {
            
            var marker = new Marker();
            if (ModelState.IsValid)
            {
                using (TipsiDbContext db = new TipsiDbContext())
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                    var currentUser = UserManager.FindById(User.Identity.GetUserId());
                    
                    if (currentUser.Marker == null)
                    {
                        jsonModel.MarkerId = currentUser.Id;
                        jsonModel.status = true;
                        jsonModel.type_icon = setIconType(jsonModel.category);
                        db.Markers.Add(jsonModel);
                        db.SaveChanges();
                    }
                    else
                    {
                        var _features = db.Features.Where(m => m.Marker.MarkerId == currentUser.Id).FirstOrDefault();
                        if (_features != null)
                            db.Features.Remove(_features);
                       
                        var _openHours = db.OpenHours.Where(m => m.Marker.MarkerId == currentUser.Id).FirstOrDefault();
                        if (_openHours != null)
                            db.OpenHours.Remove(_openHours);

                        jsonModel.type_icon = setIconType(jsonModel.category);
                        mapMyModelToDatabase(jsonModel, currentUser);
                        db.SaveChanges();
                    }
                }
                // Save data to database, and redirect to Success page.
                return RedirectToAction("MyItems");
            }
          return View(marker);
        }

        [HttpPost]
        public ActionResult RemoveItem()
        {
            var userID = User.Identity.GetUserId();

            try
            {
                using (TipsiDbContext db = new TipsiDbContext())
                {
                    var _feature = db.Features.Where(m => m.Marker.MarkerId == userID).FirstOrDefault();
                    if (_feature != null)
                        db.Features.Remove(_feature);

                    var _openHour = db.OpenHours.Where(m => m.Marker.MarkerId == userID).FirstOrDefault();
                    if (_openHour != null)
                        db.OpenHours.Remove(_openHour);
                                    
                    var _marker = db.Markers.Where(x => x.MarkerId == userID).FirstOrDefault();
                    if (_marker != null)
                        db.Markers.Remove(_marker);

                    db.SaveChanges();
                }
                return Json(new { Status = true });
            }
            catch (Exception errorMsg)
            {
                return Json(new { Status = errorMsg });
            }
        }

        public ActionResult Success()
        {
            return View();
        }
        
        public ActionResult upload()
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    bool ifProfilePic = Request.UrlReferrer.ToString().Contains("Profile");
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        //var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\WallImages", Server.MapPath(@"\")));
                        
                        string currentUserId = "";
                        using (TipsiDbContext context = new TipsiDbContext())
                        {
                            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                            var currentUser = UserManager.FindById(User.Identity.GetUserId());
                            currentUserId = currentUser.Id;


                            string galleryPath = System.IO.Path.Combine(OriginalDirectory.ToString(), currentUserId);
                            var fileName1 = Path.GetFileName(file.FileName);
                            bool isExists = System.IO.Directory.Exists(galleryPath);
                            if (!isExists)
                                System.IO.Directory.CreateDirectory(galleryPath);


                            if (ifProfilePic)
                            {
                                var profilePath = System.IO.Path.Combine(OriginalDirectory.ToString(), currentUserId, string.Format("Profile"));

                                if (!Directory.Exists(profilePath))
                                    Directory.CreateDirectory(profilePath);

                                var path = string.Format("{0}\\{1}", profilePath, file.FileName);
                                file.SaveAs(path);
                                saveGalleryPathToDB(currentUser, context, file, path, ifProfilePic);
                            }
                            else
                            {
                                var path = string.Format("{0}\\{1}", galleryPath, file.FileName);
                                file.SaveAs(path);

                                saveGalleryPathToDB(currentUser, context, file, path, ifProfilePic);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }

            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        public ActionResult delete(string name)
        {
            bool ifProfilePic = Request.UrlReferrer.ToString().Contains("Profile");

            if (!string.IsNullOrEmpty(name))
            {
                //var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\WallImages", Server.MapPath(@"\")));

                string currentUserId = "";
                using (TipsiDbContext context = new TipsiDbContext())
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var currentUser = UserManager.FindById(User.Identity.GetUserId());
                    currentUserId = currentUser.Id;
                    
                    string pathString = System.IO.Path.Combine(OriginalDirectory.ToString(), currentUserId);
                    var file = Path.GetFileName(string.Format("{0}\\{1}", pathString, name));
                    
                    if (ifProfilePic)
                    {
                        bool isPicExists = System.IO.File.Exists(string.Format("{0}\\{1}", Path.Combine(OriginalDirectory.ToString(), currentUserId, "Profile"), name));
                        if (isPicExists)
                            System.IO.File.Delete(string.Format("{0}\\{1}", Path.Combine(OriginalDirectory.ToString(), currentUserId, "Profile"), name));
                        deletePathGalleryFromDB(currentUser, context, name, ifProfilePic);
                    }
                    else
                    {
                        bool isExists = System.IO.File.Exists(string.Format("{0}\\{1}", pathString, name));
                        if (isExists)
                            System.IO.File.Delete(string.Format("{0}\\{1}", pathString, name));
                        deletePathGalleryFromDB(currentUser, context, name, ifProfilePic);
                    }
                }
            }
            return View();
        }

        public ActionResult GetAttachments()
        {
            using (TipsiDbContext context = new TipsiDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var currentUser = UserManager.FindById(User.Identity.GetUserId());
                                           
                var galleryList = new List<Gallery>();

                if(currentUser.Marker != null)
                {
                    var currentGallery = currentUser.Marker.list_gallery.Where(m=>m.isProfilePic == false);
                    if (currentGallery != null)
                    {
                        foreach (var item in currentGallery)
                        {
                            var newPath = item.Path.ToString().Replace(@"C:\Tipsi\Tipsi\", "http://localhost:5580//");
                            galleryList.Add(new Gallery()
                            {
                                FileName = item.FileName,

                                Path = newPath,
                                Size = item.Size
                            });
                        }
                    }
                }              

                //var currentGallery = currentUser.Marker.gallery.ToList();
                return Json(new { Data = galleryList }, JsonRequestBehavior.AllowGet);

            }


            //    //Get the images list from repository
            //    var attachmentsList = new List<AttachmentsModel>
            //    {
            //        new AttachmentsModel {Size = 55598, AttachmentID = 1, FileName = "/images/wallimages/1.jpg", Path = "/images/wallimages/1.jpg"},
            //        new AttachmentsModel {Size = 434343, AttachmentID = 1, FileName = "/images/wallimages/2.jpg", Path = "/images/wallimages/2.jpg"}
            //    }.ToList();

            //return Json(new { Data = attachmentsList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProfilePicture()
        {
            using (TipsiDbContext context = new TipsiDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var currentUser = UserManager.FindById(User.Identity.GetUserId());

                var prof_pictureModel = new ProfilePic();

                var currentProfPicture = currentUser.ProfilePic;
                if (currentProfPicture != null)
                {
                    var newPath = currentProfPicture.Path.ToString().Replace(@"C:\Tipsi\Tipsi\", "http://localhost:5580//");
                    prof_pictureModel.FileName = currentProfPicture.FileName;
                    prof_pictureModel.Path = newPath;
                    prof_pictureModel.Size = currentProfPicture.Size;
                }

                //var currentGallery = currentUser.Marker.gallery.ToList();
                return Json(new { prof_pictureModel }, JsonRequestBehavior.AllowGet);
            }
        }

        private void mapMyModelToDatabase(Marker jsonModel, ApplicationUser currentUser)
        {            
            var mapMarker_Config = new MapperConfiguration(cfg => cfg.CreateMap<Marker, Marker>().ForMember(x => x.MarkerId, opt => opt.Ignore())
                                                                                                   .ForMember(x => x.ApplicationUser, opt => opt.Ignore())
                                                                                                   .ForMember(x => x.ApplicationUserID, opt => opt.Ignore())
                                                                                                   //.ForMember(x => x.features, opt => opt.Ignore())
                                                                                                   //.ForMember(x => x.open_hours, opt => opt.Ignore())
                                                                                                   .ForMember(x => x.list_gallery, opt => opt.Ignore())
                                                                                                   );

            //var mapFeatures_Config = new MapperConfiguration(cfg => cfg.CreateMap<Features, Features>().ForMember(x => x.Marker, opt => opt.Ignore())
            //                                                                            .ForMember(x => x.Id, opt => opt.Ignore())
            //                                                                            .ForMember(x => x.Marker, opt => opt.Ignore())
            //                                                                            );

            //var mapOpenHours_Config = new MapperConfiguration(cfg => cfg.CreateMap<OpenHours, OpenHours>().ForMember(x => x.Id, opt => opt.Ignore())
            //                                                                                              .ForMember(x => x.Marker, opt => opt.Ignore())
            //                                                                                              );

            //var mapGallery_Config = new MapperConfiguration(cfg => cfg.CreateMap<Gallery, Gallery>().ForMember(x => x.Id, opt => opt.Ignore())
            //                                                                                        .ForMember(x => x.CurrentMarkerId, opt => opt.Ignore())
            //                                                                                              );

            var markerMapper = mapMarker_Config.CreateMapper();
            //var featuresMapper = mapFeatures_Config.CreateMapper();
            //var openHoursMapper = mapOpenHours_Config.CreateMapper();
            //var galleryMapper = mapGallery_Config.CreateMapper();

            markerMapper.Map(jsonModel, currentUser.Marker);
            //featuresMapper.Map(jsonModel.features, currentUser.Marker.features);
            //openHoursMapper.Map(jsonModel.open_hours, currentUser.Marker.open_hours);
            //galleryMapper.Map(jsonModel.gallery, currentUser.Marker.gallery);            
        }

        private Marker mapMyModelToView(ApplicationUser currentUser, Marker jsonModel)
        {
            var mapMarker_Config = new MapperConfiguration(cfg => cfg.CreateMap<Marker, Marker>());
            var markerMapper = mapMarker_Config.CreateMapper();         

            markerMapper.Map(currentUser.Marker, jsonModel);
            return jsonModel;
        }

        private void saveGalleryPathToDB(ApplicationUser currentUser, TipsiDbContext context,  HttpPostedFileBase file, string path, bool ifProfilePic)
        {
            path = path.Replace(@"C:\Tipsi\Tipsi\", @"\").Replace(@"\", @"/");

            //if ifProfilePic is true then save to different place
            if (ifProfilePic)
            {
                var oldPicture = currentUser.ProfilePic;
                if (oldPicture != null)
                    context.ProfilePictures.Remove(oldPicture);

                var _newProfPic = new ProfilePic()
                {
                    FileName = file.FileName,
                    Path = path,
                    Size = file.ContentLength,
                    CurrentUser = currentUser
                };
                context.ProfilePictures.Add(_newProfPic);
            }
            else
            {
                List<Gallery> newPicturePath = new List<Gallery>()
                    {
                        new Gallery()
                        {
                            FileName = file.FileName,
                            Path = path,
                            Size = file.ContentLength,
                            CurrentMarkerId = currentUser.Id,
                            isProfilePic = ifProfilePic
                        }
                     };

                if (currentUser.Marker == null)
                {
                    currentUser.Marker = new Marker()
                    {
                        MarkerId = currentUser.Id,
                        //list_gallery = newPicturePath,
                        name = "",
                        phone_number = "",
                        email = "",
                        status = false
                    };
                    currentUser.Marker.list_gallery = newPicturePath;
                }
                else
                {
                    currentUser.Marker.list_gallery.Add(new Gallery()
                    {
                        FileName = file.FileName,
                        Path = path,
                        Size = file.ContentLength,
                        CurrentMarkerId = currentUser.Id,
                        isProfilePic = ifProfilePic
                    });
                }                
            }
            context.SaveChanges();                        
        }

        private void deletePathGalleryFromDB(ApplicationUser currentUser, TipsiDbContext context, string name, bool ifProfilePic)
        {
            if (ifProfilePic)
            {
                var oldPicture = currentUser.ProfilePic;
                if (oldPicture != null)
                    context.ProfilePictures.Remove(oldPicture);
                context.SaveChanges();
            }
            else
            {
                var galleryForDelete = context.Galleries.Where(m => m.CurrentMarkerId == currentUser.Id).Where(x => x.FileName == name);
                if (galleryForDelete != null)
                {
                    context.Galleries.RemoveRange(galleryForDelete);
                    context.SaveChanges();
                }
            }             
        }

        private async Task<string> SendEmailConfirmationWarningAsync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(userID, subject,
               "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            return callbackUrl;
        }

        public async Task<ActionResult> CancelUnconfirmedEmail(string UserId)
        {
            var user = await UserManager.FindByEmailAsync(UserId);
            if (user == null)
            {
                user = await UserManager.FindByIdAsync(UserId);
                if (user != null)
                {
                    user.UnConfirmedEmail = "";
                    user.EmailConfirmed = true;
                    var result = await UserManager.UpdateAsync(user);
                }
            }
            else
            {
                user.UnConfirmedEmail = "";
                user.EmailConfirmed = true;
                var result = await UserManager.UpdateAsync(user);
            }
            return RedirectToAction("Profile", "Users");
        }

        private string setIconType(string category)
        {
            string iconPath = "assets/icons/cosmetics/offer.png";

            switch (category)
            {
                case "Salon Kosmetyczny":
                    iconPath = "assets/icons/cosmetics/cosmetics.png";
                    break;
                case "Fryzjer":
                    iconPath = "assets/icons/cosmetics/scissor.png";
                    break;
                case "Barber":
                    iconPath = "assets/icons/cosmetics/barber.png";
                    break;
                case "Spa":
                    iconPath = "assets/icons/cosmetics/spa.png";
                    break;
                case "Salon Masażu":
                    iconPath = "assets/icons/cosmetics/massage.png";
                    break;
                case "Ogłoszenie sąsiedzkie":
                    iconPath = "assets/icons/cosmetics/offer.png";
                    break;
                default:
                    iconPath = "assets/icons/cosmetics/offer.png";
                    break;
            }
            return iconPath;
        }

    }
}